import C from '../constants';
import { fetchMessages, atMoment } from '../helpers';
import { v4 as uuid } from 'uuid';

export const loadMessagesAction = () => async dispatch => {
  try {
    const messages = await fetchMessages();

    dispatch({
      type: C.SET_MESSAGES,
      payload: messages
    });
  } catch (error) {
    throw error;
  }
};

export const addMessageAction = message => ({
  type: C.ADD_MESSAGE,
  payload: {
    ...message,
    id: uuid(),
    avatar: '',
    user: 'User',
    createdAt: atMoment(),
    likes: [],
    editedAt: ''
  }
});

export const deleteMessageAction = messageId => ({
  type: C.DELETE_MESSAGE,
  payload: messageId
});

export const updateMessageAction = message => ({
  type: C.UPDATE_MESSAGE,
  payload: message
});

export const likeMessageAction = (message, userId) => ({
  type: C.LIKE_MESSAGE,
  payload: { message, userId }
});
