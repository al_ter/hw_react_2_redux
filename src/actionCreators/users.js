import C from '../constants';

export const addUserAction = user => ({
  type: C.ADD_USER,
  payload: user
});

export const deleteUserAction = userId => ({
  type: C.DELETE_USER,
  payload: userId
});

export const updateUserAction = user => ({
  type: C.UPDATE_USER,
  payload: user
});

export const setCurrentUserAction = userId => ({
  type: C.SET_CURRENT_USER,
  payload: userId
});
