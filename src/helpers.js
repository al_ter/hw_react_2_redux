import { MESSAGES_URL } from './config';

export const fetchMessages = async () => {
  const response = await fetch(MESSAGES_URL);
  const messages = await response.json();
  return messages;
};

export const getUniquesByField = array => {
  if (!array.length) return;
  const unique = new Set();

  array.forEach(item => unique.add(item.userId));

  return unique.size; 
};

export const sortByDate = array => array.sort((a, b) => Date(a.createdAt) - Date(b.createdAt));

export const addLike = (message, userId) => {
  if (!message.likes || message.likes.length === 0) {
    return { ...message, likes: [ userId ] }
  }

  const idx = message.likes.findIndex(v => v === userId);
  if (idx !== -1) return message;

  return {
    ...message,
    likes: [
      ...message.likes,
      userId
    ]
  }
};

export const findById = (array, id) => array.find(item => item.id === id);

export const findIdxByField = (array, field, value) => array.findIndex(item => item[field] === value);

export const findLastUserMessage = (array, userId) => {
  for (let i = array.length - 1; i >= 0; i--) {
    if (array[i].userId === userId) return array[i];
  }
};

export const atMoment = () => new Date().toISOString();

export const scrollToBottom = () => window.scrollTo(0, document.body.scrollHeight);

export const focusOnElement = selector => {
  if (!selector) return;

  const element = document.querySelector(`${selector}`);
  element.focus();
};
