import C from '../constants';

const initialState = {
  users: [],
  currentUserId: null
};

const usersReducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case C.SET_USERS:
      return { ...state, users: payload };

    case C.ADD_USER:
      return { ...state, users: [ ...state.users, payload ] };

    case C.DELETE_USER:
      return {
        ...state,
        users: state.users.filter(user => user.id === payload ? false : true)
      };

    case C.SET_CURRENT_USER:
      return { ...state, currentUserId: payload };

    default: return state
  }
};

export default usersReducer;
