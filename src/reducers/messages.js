import C from '../constants';
import { sortByDate, atMoment, addLike } from '../helpers';

const messagesReducer = (state = [], action) => {
  const { type, payload } = action;
  let newState = [];

  switch (type) {
    case C.SET_MESSAGES:
      return sortByDate(payload);

    case C.ADD_MESSAGE:
      return sortByDate([...state, payload]);

    case C.UPDATE_MESSAGE:
      newState = state.map(message => (
        message.id === payload.id
          ? { ...payload, editedAt: atMoment() }
          : message
      ));
      return sortByDate(newState);

    case C.DELETE_MESSAGE:
      newState = state.filter(message => message.id === payload ? false : true);
      return sortByDate(newState);

    case C.LIKE_MESSAGE:
      newState = state.map(message => (
        message.id === payload.message.id
          ? addLike(payload.message, payload.userId)
          : message
      ));
      return sortByDate(newState);

    default: return state
  }
};

export default messagesReducer;
