import React, { useState, useEffect } from 'react';
import { focusOnElement } from '../../helpers';

const Modal = ({ message, onMessageSend, onClose }) => {
  const [ text, setText ] = useState('');
  const messageInputId = 'message-text';

  useEffect(() => {
    if (message) {
      setText(message.text);
    }
    focusOnElement(`#${messageInputId}`);
  }, [message]);

  const handleOnInput = ({ target: { value } }) => setText(value);

  const handleOnSubmit = e => {
    e.preventDefault();

    if (!text.trim()) return;
    
    onMessageSend({ ...message, text });
    setText('');
  };

  return (
    <div className='modal' tabIndex='-1'>
      <div className='modal-dialog modal-dialog-centered'>
        <div className='modal-content'>
            <div className='modal-header'>
            <h5 className='modal-title'>Edit message</h5>
            <button type='button' className='btn-close' data-bs-dismiss='modal' aria-label='Close' onClick={onClose}></button>
          </div>
          <div className='modal-body'>
            <form onSubmit={handleOnSubmit}>
              <div className='mb-3'>
                <input
                  className='form-control'
                  id={`${messageInputId}`}
                  onInput={handleOnInput}
                  value={text}
                />
              </div>
              <div className='modal-footer'>
                <button type='button' className='btn btn-secondary' data-bs-dismiss='modal' onClick={onClose}>Cancel</button>
                <button type='submit' className='btn btn-primary'>Save changes</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Modal;
