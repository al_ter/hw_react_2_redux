import React from 'react';
import { connect } from 'react-redux';
import Message from '../Message';
import { deleteMessageAction, likeMessageAction } from '../../actionCreators/messages';

const MessageList = ({
  messages,
  currentUserId,
  likeMessageAction: onLike,
  deleteMessageAction: onDelete,
  onEdit
}) => {
  let prevTimestamp = '';

  const doWeNeedDivider = nextTimestamp => {
    if (!prevTimestamp) {
      prevTimestamp = nextTimestamp;
      return nextTimestamp;
    }

    const prev = new Date(prevTimestamp);
    const next = new Date(nextTimestamp);
    const prevDate = prev.getDate();
    const prevMonth = prev.getMonth();
    const prevYear = prev.getFullYear();
    const nextDate = next.getDate();
    const nextMonth = next.getMonth();
    const nextYear = next.getFullYear();

    prevTimestamp = nextTimestamp;

    if (
      prevDate !== nextDate ||
      prevMonth !== nextMonth ||
      prevYear !== nextYear
    ) {
      return nextTimestamp;
    }

    return false;
  };

  return (
    <div className='container message-list py-5'>
      {
        messages.map(message => (
            <Message
              key={message.id}
              message={message}
              currentUserId={currentUserId}
              onLike={onLike}
              onDelete={onDelete}
              onEdit={onEdit}
              dividerTimestamp={doWeNeedDivider(message.createdAt)}
            />
          )
        )
      }
    </div>
  );
};

export default connect(null, { likeMessageAction, deleteMessageAction })(MessageList);
