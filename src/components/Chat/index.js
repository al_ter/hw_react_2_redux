import React, { useState, useEffect, useCallback } from 'react';
import { connect } from 'react-redux';
import Spinner from '../Spinner';
import Header from '../Header';
import MessageList from '../MessageList';
import MessageInput from '../MessageInput';
import Modal from '../Modal';
import { getUniquesByField, findById, scrollToBottom, findLastUserMessage } from '../../helpers';
import { v4 as uuid } from 'uuid';

import { loadMessagesAction, updateMessageAction } from '../../actionCreators/messages';
import { setCurrentUserAction } from '../../actionCreators/users';

const Chat = ({
  messages,
  currentUserId: userId,
  loadMessagesAction: loadMessages,
  updateMessageAction: updateMessage,
  setCurrentUserAction: setCurrentUser
}) => {
  const [isLoading, setIsLoading] = useState(true);
  const [messageInEditor, setMessageInEditor] = useState({});
  let lastMessage = messages[messages.length - 1];

  const onEdit = messageId => {
    setMessageInEditor(findById(messages, messageId));
  };

  const onMessageUpdate = message => {
    updateMessage(message);
    setMessageInEditor('');
  };

  const onClose = () => {
    setMessageInEditor('');
  };

  useEffect(() => {
    setCurrentUser(uuid());
  }, [setCurrentUser]);

  useEffect(() => {
    loadMessages();
  }, [loadMessages]);

  useEffect(() => {
    messages.length && setIsLoading(false);
  }, [messages]);

  const handleKeyDown = useCallback(e => {
      if (e.key !== 'ArrowUp') return;
      
      e.preventDefault();

      const lastUserMessage = findLastUserMessage(messages, userId);
      lastUserMessage && setMessageInEditor(lastUserMessage);
    }, [messages, userId]);

  useEffect(() => {
    if (!messages.length) return;

    window.addEventListener('keydown', handleKeyDown);

    return () => {
      window.removeEventListener('keydown', handleKeyDown);
    };
  }, [messages, handleKeyDown]);

  useEffect(() => {
    scrollToBottom();
  }, [messages]);

  return (
    isLoading
      ? <Spinner />
      : (
        <>
          <main className='content'>
            <div className='container chat'>
              <Header
                chatName='Chat name'
                usersCount={getUniquesByField(messages)}
                messagesCount={messages.length}
                messagesLastAt={lastMessage && lastMessage.createdAt}
              />
              <MessageList
                messages={messages}
                currentUserId={userId}
                onEdit={onEdit}
              />
              <MessageInput
                currentUserId={userId}
              />
            </div>
          </main>
          {
            messageInEditor.id &&
            <Modal
              message={messageInEditor}
              onMessageSend={onMessageUpdate}
              onClose={onClose}
            />
          }
        </>
      )
  );
}

const mapStateToProps = ({ messages, users: { currentUserId } }) => {
  return { messages, currentUserId };
}

export default connect(mapStateToProps, { loadMessagesAction, updateMessageAction, setCurrentUserAction })(Chat);
