import React, { useState } from 'react';
import { connect } from 'react-redux';
import { addMessageAction } from '../../actionCreators/messages';

const MessageInput = ({ currentUserId, addMessageAction: onMessageSend }) => {
  const [ text, setText ] = useState('');

  const handleOnInput = ({ target: { value } }) => setText(value);

  const handleOnSubmit = e => {
    e.preventDefault();

    if (!text.trim()) return;

    onMessageSend({ userId: currentUserId, text });
    setText('');
  };

  return (
    <div className='message-input py-3 px-4 border-top'>
      <form
        id='message-form'
        className='input-group'
        onSubmit={handleOnSubmit}
      >
        <input
          type='text'
          className='form-control'
          placeholder='Type your message'
          onInput={handleOnInput}
          value={text}
        />
        <button className='btn btn-primary'>
          Send
        </button>
      </form>
    </div>
  );
}

export default connect(null, { addMessageAction })(MessageInput);
