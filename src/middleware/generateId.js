import { v4 as uuid } from 'uuid';
import C from '../constants';

const generateIdMiddleware = store => next => action => {
  if (action.type === C.ADD_MESSAGE) {
    action = {
      ...action,
      payload: {
        ...action.payload,
        id: uuid()
      }
    };
  }

  return next(action);
};

export default generateIdMiddleware;
